package com.miraclesea.framework.dao;

import com.miraclesea.framework.entity.TestEntity;

public interface TestEntityDao extends BaseJpaRepository<TestEntity, String> {

}
