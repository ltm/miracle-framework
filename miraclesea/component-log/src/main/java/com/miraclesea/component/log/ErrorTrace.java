package com.miraclesea.component.log;

import org.aspectj.lang.ProceedingJoinPoint;

import com.miraclesea.framework.lang.Constants;

class ErrorTrace {
	
	private final Throwable e;
	private final CallTrace callTrace;
	
	ErrorTrace(final Throwable e, final ProceedingJoinPoint pjp) {
		this.e = e;
		callTrace = new CallTrace(pjp);
	}
	
	String getExceptionInfo() {
		StringBuilder result = new StringBuilder();
		result.append(String.format("Got an unexpected exception. It is: %s", e));
		result.append(Constants.LINE_SEPARATOR);
		result.append("Replay the method call scenario...");
		result.append(Constants.LINE_SEPARATOR);
		result.append(callTrace.getSignatureInfo());
		result.append(Constants.LINE_SEPARATOR);
		result.append(callTrace.getArgumentsInfo());
		result.append(Constants.LINE_SEPARATOR);
		return result.toString();
	}
	
	Throwable getException() {
		return e;
	}
}
