package com.miraclesea.component.log.asserts;

public final class Arguments {
	
	private final Object[] arguments;
	
	public Arguments(final Object... arguments) {
		this.arguments = arguments;
	}
	
	public int getLength() {
		return arguments.length;
	}
	
	public boolean hasArguments() {
		return getLength() > 0;
	}
	
	public String getTypes() {
		StringBuilder result = new StringBuilder();
		int length = getLength();
		int count = 0;
		for (Object each : arguments) {
			result.append(each.getClass().getName());
			if (count < length - 1) {
				result.append(",");
			}
			count++;
		}
		return result.toString();
	}
	
	public String getValues() {
		StringBuilder result = new StringBuilder();
		int length = getLength();
		int count = 0;
		for (Object each : arguments) {
			result.append(count + 1);
			result.append(" -> ");
			result.append(each);
			if (count < length - 1) {
				result.append(", ");
			} else {
				result.append(" ");
			}
			count++;
		}
		return result.toString();
	}
}
