package com.miraclesea.component.log;

import static com.miraclesea.component.log.asserts.LogAssert.assertError;
import static com.miraclesea.component.log.asserts.LogAssert.assertPerformance;
import static com.miraclesea.component.log.asserts.LogAssert.assertTrace;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.unitils.thirdparty.org.apache.commons.io.FileUtils;

import com.miraclesea.component.log.asserts.MethodInstance;
import com.miraclesea.framework.exception.LogicException;
import com.miraclesea.framework.exception.UserException;
import com.miraclesea.test.integrate.SpringBaseTest;

public final class LoggerAspectTest extends SpringBaseTest {
	
	private static final String LOG_FILE_FOLDER = "target/test-classes/";
	private static final String TRACE_LOG_FILE = "Trace.log";
	private static final String PERFORMANCE_LOG_FILE = "Performance.log";
	private static final String ERROR_LOG_FILE = "Error.log";
	
	@Resource
	private TestLog testLog;
	
	@Test
	public void voidAndNoParameter() {
		testLog.voidAndNoParameter();
		MethodInstance methodInstnce = new MethodInstance("voidAndNoParameter", void.class, "null");
		assertTrace(readLogFile(TRACE_LOG_FILE), methodInstnce);
		assertPerformance(readLogFile(PERFORMANCE_LOG_FILE), methodInstnce);
	}
	
	@Test
	public void voidAndOneParameter() {
		testLog.voidAndOneParameter(1);
		MethodInstance methodInstnce = new MethodInstance("voidAndOneParameter", void.class, "null", 1);
		assertTrace(readLogFile(TRACE_LOG_FILE), methodInstnce);
		assertPerformance(readLogFile(PERFORMANCE_LOG_FILE), methodInstnce);
	}
	
	@Test
	public void hasReturnValueAndTwoParameter() {
		testLog.hasReturnValueAndTwoParameter("s", 1);
		MethodInstance methodInstnce = new MethodInstance("hasReturnValueAndTwoParameter", List.class, "[A, B, C]", "s", 1);
		assertTrace(readLogFile(TRACE_LOG_FILE), methodInstnce);
		assertPerformance(readLogFile(PERFORMANCE_LOG_FILE), methodInstnce);
	}
	
	@Test
	public void voidAndNoParameterThrowUserException() {
		try {
			testLog.voidAndNoParameterThrowUserException();
		} catch (final UserException ex) {
			MethodInstance methodInstnce = new MethodInstance("voidAndNoParameterThrowUserException", void.class, ex);
			assertTrace(readLogFile(TRACE_LOG_FILE), methodInstnce);
			assertPerformance(readLogFile(PERFORMANCE_LOG_FILE), methodInstnce);
		}
	}
	
	@Test
	public void voidAndOneParameterThrowLogicException() {
		try {
			testLog.voidAndOneParameterThrowLogicException(1L);
		} catch (final LogicException ex) {
			MethodInstance methodInstnce = new MethodInstance("voidAndOneParameterThrowLogicException", void.class, ex, 1L);
			assertTrace(readLogFile(TRACE_LOG_FILE), methodInstnce);
			assertPerformance(readLogFile(PERFORMANCE_LOG_FILE), methodInstnce);
		}
	}
	
	@Test
	public void hasReturnValueAndTwoParameterThrowException() {
		try {
			testLog.hasReturnValueAndTwoParameterThrowException(1, "s");
		} catch (final IllegalStateException ex) {
			MethodInstance methodInstnce = new MethodInstance("hasReturnValueAndTwoParameterThrowException", String.class, ex, 1, "s");
			assertError(readLogFile(ERROR_LOG_FILE), methodInstnce);
			assertPerformance(readLogFile(PERFORMANCE_LOG_FILE), methodInstnce);
		}
	}
	
	private String readLogFile(final String logFileName) {
		try {
			return FileUtils.readFileToString(new File(LOG_FILE_FOLDER + logFileName), "UTF-8");
		} catch (final IOException ex) {
			throw new RuntimeException(ex);
		}
	}
}
