package com.miraclesea.component.log;

import java.util.List;

import com.miraclesea.framework.exception.LogicException;

public interface TestLog {
	
	void voidAndNoParameter();
	
	void voidAndOneParameter(Integer para);
	
	List<String> hasReturnValueAndTwoParameter(String para1, Integer para2);
	
	void voidAndNoParameterThrowUserException();
	
	void voidAndOneParameterThrowLogicException(Long para) throws LogicException;
	
	String hasReturnValueAndTwoParameterThrowException(Integer para1, String para2);
}
