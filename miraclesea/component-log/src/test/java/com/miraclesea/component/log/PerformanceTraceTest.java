package com.miraclesea.component.log;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.junit.Test;
import org.mockito.Mock;

import com.miraclesea.test.integrate.MockBaseTest;

public final class PerformanceTraceTest extends MockBaseTest {
	
	@Mock
	private ProceedingJoinPoint pjp;
	
	@Mock
	private Signature signature;
	
	@Test
	public void getPerformaceInfo() throws InterruptedException {
		when(signature.toLongString()).thenReturn("public void xxx");
		when(pjp.getSignature()).thenReturn(signature);
		PerformanceTrace performanceTrace = new PerformanceTrace(pjp);
		Thread.sleep(1000);
		String actual = performanceTrace.getPerformanceInfo();
		String expected = "Method signature: public void xxx, Spend time is: (\\d+) nanos, about (\\d+) millis, about (\\d+) seconds.";
		Matcher matcher = Pattern.compile(expected).matcher(actual);
		assertTrue(matcher.matches());
		assertSpendNanos(Long.parseLong(matcher.group(1)));
		assertSpendMillis(Long.parseLong(matcher.group(2)));
		assertSpendSeconds(Long.parseLong(matcher.group(3)));
		verify(signature).toLongString();
		verify(pjp).getSignature();
	}
	
	private void assertSpendNanos(final long spendNanos) {
		assertTrue(spendNanos > 0);
	}
	
	private void assertSpendMillis(final long spendMillis) {
		assertTrue(spendMillis >= 0);
	}
	
	private void assertSpendSeconds(final long spendSeconds) {
		assertTrue(spendSeconds >= 0);
	}
}
