package com.miraclesea.test.webmvc;

import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public final class MockHttpRequestBuilder {
	
	private MockHttpRequestBuilder() { }
	
	public static void clearRequestContextHolder() {
		RequestContextHolder.setRequestAttributes(null);
	}
	
	public static MockHttpServletRequest getMockHttpRequest() {
		MockHttpServletRequest result = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(result));
		return result;
	}
}
