package com.miraclesea.module.rbac.entity;

import com.miraclesea.framework.entity.BaseOptimisticLockAuditable;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "t_user")
public final class User extends BaseOptimisticLockAuditable<User> {
	
	private static final long serialVersionUID = 3614563091717547065L;
	
}
