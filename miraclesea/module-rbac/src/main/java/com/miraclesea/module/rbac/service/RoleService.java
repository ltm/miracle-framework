package com.miraclesea.module.rbac.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.miraclesea.module.rbac.entity.Role;

public interface RoleService {
	
	void create(Role role);
	void update(Role role);
	void delete(String id);
	Role load(String id);
	Page<Role> findAll(Pageable pageable);
	boolean isUsedRoleName(Role role);
}
